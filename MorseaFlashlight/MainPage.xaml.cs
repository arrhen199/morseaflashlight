﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Foundation.Metadata;
using Windows.Graphics.Display;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Devices.Lights;
using Windows.UI.Popups;
using MorseaFlashlight.Morsea;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Windows.Input;
using System.Text;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.Media.Playback;
using System.Reflection;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MorseaFlashlight
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private static string DisabledSoundIcon = "\uE74f";
        private static string EnabledSoundIcon = "\uE767";

        private SolidColorBrush Black { get; set; }
        private SolidColorBrush Yellow { get; set; }
        private string LowerBrightness { get; set; }
        private string HigherBrightness { get; set; }

        public Controller Controller { get; set; }
        public CancellationToken CancelToken { get; set; }
        public CancellationTokenSource CancelTokenSource { get; set; }
        public Task CurrentTask { get; set; }
        public Random rnd;

        public MediaElement MediaPlayer { get; set; }
        public StorageFile SoundFile { get; set; }
        IRandomAccessStream Stream { get; set; }

        public Lamp Lamp { get; set; }


        public MainPage()
        {
            this.InitializeComponent();
            initGuiTheme();
            rnd = new Random();
            checkDevice();
            //Controller = new Controller();
            CancelTokenSource = new CancellationTokenSource();
            CancelToken = CancelTokenSource.Token;
            SetSoundElement();


        }
        public async void SetSoundElement()
        {

            var folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(@"Assets");
            SoundFile = await folder.GetFileAsync("signalSound");
            Stream = await SoundFile.OpenAsync(Windows.Storage.FileAccessMode.Read);

        }

        private async void checkDevice()
        {
            Lamp = await Lamp.GetDefaultAsync();
            if (Lamp == null)
            {
                var dialog = new MessageDialog("Your device is not supported");
                await dialog.ShowAsync();
                Application.Current.Exit();
            }
            else
            {
                Flashlight flashlight = new Flashlight();
                flashlight.Lamp = Lamp;
                flashlight.Brightness = Lamp.BrightnessLevel;
                Controller = new Controller(flashlight);
            }
            //Controller.FlashLightInstance.Lamp = Lamp;

            var folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(@"Assets");
            var sound = await folder.GetFileAsync("signalSound");
            var stream = await SoundFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
            BackgroundMediaPlayer.Current.AutoPlay = false;
            BackgroundMediaPlayer.Current.IsLoopingEnabled = true;
            BackgroundMediaPlayer.Current.SetStreamSource(stream);


        }

        public void initGuiTheme()
        {
            Yellow = new SolidColorBrush(Colors.Yellow);
            Black = new SolidColorBrush(Colors.Black);

            flashbutton.Foreground = Black;
            repeatButton.Foreground = Black;
            soundButton.Foreground = Black;
            lightButton.Foreground = Yellow;
            MySplitView.OpenPaneLength = GetScreenResolutionInfo().Width;
            setTitleBar();
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
            if (MySplitView.IsPaneOpen)
                setInitData();
            else
                clearData();
        }

        private void setInitData()
        {
            ListBoxMorseSignals.ItemsSource = Controller.UserSignals;
        }
        private void clearData()
        {
            ListBoxMorseSignals.ItemsSource = null;
        }

        private Size GetScreenResolutionInfo()
        {
            var applicationView = ApplicationView.GetForCurrentView();
            var displayInformation = DisplayInformation.GetForCurrentView();
            var bounds = applicationView.VisibleBounds;
            var scale = displayInformation.RawPixelsPerViewPixel;
            var size = new Size(bounds.Width * scale, bounds.Height * scale);
            return size;
        }

        private void flashbutton_Click(object sender, RoutedEventArgs e)
        {
            if (flashbutton.Foreground == Black)
            {
                flashbutton.Foreground = Yellow;
                if (CurrentTask != null)
                    CancelTokenSource.Cancel();
                Controller.FlashOn();
            }
            else
            {
                Controller.FlashOff();
                flashbutton.Foreground = Black;
                if (CurrentTask != null)
                    CancelTokenSource.Cancel();
            }
        }

        private void signalButton_Click(object sender, RoutedEventArgs e)
        {
            var text = txtboxSignal.Text.ToLower();
            setSignalText(text);
            if (CurrentTask != null && CurrentTask.Status == TaskStatus.Running)
            {
                Controller.Abort();

                CancelTokenSource.Cancel();
                Debug.WriteLine("Task cancelled : " + CurrentTask.Id + "  " + CurrentTask.Status);

                //DateTime start = DateTime.Now;
                //while (DateTime.Now.Subtract(start).Milliseconds < (500))
                //{

                //}
            }

            CancelTokenSource = new CancellationTokenSource();
            CancelToken = CancelTokenSource.Token;

            CurrentTask = new Task(() =>
            {
                Controller.Flash(text, CancelToken, MediaPlayer);
            }, CancelToken);

            CurrentTask.Start();
        }

        private void setTitleBar()
        {
            if (ApiInformation.IsTypePresent("Windows.UI.ViewManagement.ApplicationView"))
            {
                var titleBar = ApplicationView.GetForCurrentView().TitleBar;
                if (titleBar != null)
                {
                    titleBar.ButtonBackgroundColor = Colors.Black;
                    titleBar.ButtonForegroundColor = Colors.Black;
                    titleBar.BackgroundColor = Colors.Black;
                    titleBar.ForegroundColor = Colors.White;
                }
            }

            //Mobile customization
            if (ApiInformation.IsTypePresent("Windows.UI.ViewManagement.StatusBar"))
            {

                var statusBar = StatusBar.GetForCurrentView();
                if (statusBar != null)
                {
                    statusBar.BackgroundOpacity = 1;
                    statusBar.BackgroundColor = Colors.Black;
                    statusBar.ForegroundColor = Colors.White;
                }
            }
        }

        private void brigtnessButtonPlus_Click(object sender, RoutedEventArgs e)
        {
            Controller.SetBrighter();
        }

        private void brigtnessButtonMinus_Click(object sender, RoutedEventArgs e)
        {
            Controller.SetDarker();
        }

        private void addSignalButton_Click(object sender, RoutedEventArgs e)
        {
            var text = txtboxSignal.Text;
            if (text.Length > 0)
            {
                Controller.AddSignal(text);
            }
        }
        private void removeSignalButton_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            Controller.RemoveSignal((String)button.CommandParameter);
            ListBoxMorseSignals.ItemsSource = null;
            ListBoxMorseSignals.ItemsSource = Controller.UserSignals;
        }



        private void setSignalText(string text)
        {
            StringBuilder sb = new StringBuilder();
            var signal = Signal.GetInstane();

            foreach (var letter in text.ToCharArray())
            {
                foreach (var item in signal.GetSignal(letter))
                {
                    if (item == signal.LongSignal)
                        sb.Append("-");
                    else if (item > signal.LongSignal)
                        sb.Append("   ");
                    else
                        sb.Append(".");
                }
            }
            textblockMorsSignal.Text = sb.ToString();
        }

        private void contbut_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var txtbBlock = (TextBlock)sender;
            var text = txtbBlock.Text;
            setSignalText(text.ToLower());
            txtboxSignal.Text = text;
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
            clearData();
        }

        private void flashSpeedButtonPlus_Click(object sender, RoutedEventArgs e)
        {
            Controller.FlashFaster();
        }

        private void flashSpeedButtonMinus_Click(object sender, RoutedEventArgs e)
        {
            Controller.FlashSlower();
        }

        private void txtboxSignal_TextChanged(object sender, TextChangedEventArgs e)
        {
            var text = txtboxSignal.Text.ToLower();
            setSignalText(text.ToLower());
        }

        private void repeatButton_Click(object sender, RoutedEventArgs e)
        {
            if (repeatButton.Foreground == Black)
            {
                repeatButton.Foreground = Yellow;
                Controller.IsRepeat = true;

            }
            else
            {
                repeatButton.Foreground = Black;
                Controller.IsRepeat = false;

            }
        }

        private async void soundButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.isSoundEnabled = !Controller.isSoundEnabled;
            if (Controller.isSoundEnabled)
            {
                soundButton.Content = EnabledSoundIcon;
                MySplitView.Background = new SolidColorBrush(PickBrush());
                SignalsStackPanel.Background = new SolidColorBrush(PickBrush());
            }
            else
            {
                soundButton.Content = DisabledSoundIcon;
                MySplitView.Background = new SolidColorBrush(PickBrush());
                SignalsStackPanel.Background = new SolidColorBrush(PickBrush());
            }

        }
        private Color PickBrush()
        {
            Color result = Colors.Transparent;



            Type brushesType = typeof(Colors);

            PropertyInfo[] properties = brushesType.GetProperties();

            int random = rnd.Next(properties.Length);
            result = (Color)properties[random].GetValue(null, null);

            return result;
        }

        private void lightButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.isLightEnabled = !Controller.isLightEnabled;
            if (Controller.isLightEnabled)
                lightButton.Foreground = Yellow;
            else
                lightButton.Foreground = Black;
        }

        private void playSoundButton_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            if (Controller.isSoundEnabled)
                BackgroundMediaPlayer.Current.Play();
        }

        private void playSoundButton_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            BackgroundMediaPlayer.Current.Pause();
        }

        private void lighttheLampButton_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Controller.FlashOn();
        }

        private void lighttheLampButton_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            Controller.FlashOff();
        }
    }
}
