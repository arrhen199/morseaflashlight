﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Lights;

namespace MorseaFlashlight.Morsea
{
    public class Flashlight : IFlashLight
    {
        public Lamp Lamp { get; set; }
        public double Brightness { get; set; }

        public Flashlight()
        {
            
        }

        public void TurnOn()
        {
            Lamp.IsEnabled = true;
        }

        public void TurnOff()
        {
            Lamp.IsEnabled = false;
        }

        public void SetBrighter()
        {
            Lamp.BrightnessLevel += 0.1f;

        }

        public double GetBrightness()
        {
            return Lamp.BrightnessLevel;
        }

        public void SetDarker()
        {

            Lamp.BrightnessLevel -= 0.1f;

        }
    }
}
