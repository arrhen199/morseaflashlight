﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseaFlashlight.Morsea
{
    public class UserSignal
    {
        public string Content { get; set; }

        public UserSignal(string content)
        {
            this.Content = content;
        }
    }
}
