﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Storage;

namespace MorseaFlashlight.Morsea
{
    public class FileProvider
    {
        private StorageFolder _localFolder;
        private ApplicationDataContainer _localSettings;


        public FileProvider()
        {
            _localFolder = ApplicationData.Current.LocalFolder;
            _localSettings = ApplicationData.Current.LocalSettings;
        }
        public async void LoadData(Collection<UserSignal> signals)
        {
            try
            {
                StorageFile dataFile = await _localFolder.GetFileAsync("signalsFile.txt");
                //var data = await FileIO.ReadTextAsync(dataFile);
                var buffer = await FileIO.ReadBufferAsync(dataFile);
                string data = string.Empty;

                using (var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(buffer))
                {
                    data = dataReader.ReadString(buffer.Length);
                }
                string[] lines = Regex.Split(data, "\r\n");

                if (lines.Length > 0)
                {
                    signals.Clear();
                    foreach (var line in lines)
                    {
                        if (line.Length > 0)
                            signals.Add(new UserSignal(line));
                    }
                }


                // Data is contained in timestamp
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

        }
        public async void SaveData(IEnumerable<UserSignal> signals)
        {
            StorageFile sampleFile = await _localFolder.CreateFileAsync("signalsFile.txt", CreationCollisionOption.ReplaceExisting);
            StringBuilder sb = new StringBuilder();
            foreach (var signal in signals)
            {
                sb.AppendLine(signal.Content);
            }
            await FileIO.WriteTextAsync(sampleFile, sb.ToString());

            var savedTickets = await FileIO.ReadTextAsync(sampleFile);
        }
    }
}
