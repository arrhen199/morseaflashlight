﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Lights;
using Windows.Media.Playback;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace MorseaFlashlight.Morsea
{
    public class Controller
    {
        private double TimeSpan { get; set; } // step of speeding ?? 
        public IFlashLight FlashLightInstance { get; set; }
        public bool isConstantLightOn { get; set; }
        public bool isMessageFlashing { get; set; }
        public bool isAborted { get; set; }
        public Collection<UserSignal> UserSignals { get; set; }
        public bool IsRepeat { get; set; }
        public FileProvider FileProvider { get; set; }

        public MediaElement MediaPlayer { get; set; }
        public StorageFile SoundFile { get; set; }

        public double FlashSpan { get; set; } //time between signals
        public int TimespanBetweenRepeats { get; set; }

        private SolidColorBrush Black { get; set; }
        private SolidColorBrush Yellow { get; set; }
        public bool isSoundEnabled { get; set; }
        public bool isLightEnabled { get; set; }


        public Controller(IFlashLight flashlight)
        {
            FileProvider = new FileProvider();
            TimeSpan = 0.1;
            this.FlashLightInstance = flashlight;
            isAborted = false;
            UserSignals = new ObservableCollection<UserSignal>();
            InitSignals();
            FlashSpan = 1;
            TimespanBetweenRepeats = 3;
            Yellow = new SolidColorBrush(Colors.Yellow);
            Black = new SolidColorBrush(Colors.Black);
            isSoundEnabled = false;
            isLightEnabled = true;
        }


        private void InitSignals()
        {
            UserSignals.Add(new UserSignal("sos"));
            UserSignals.Add(new UserSignal("powtórz"));
            UserSignals.Add(new UserSignal("zrozumiano"));
            UserSignals.Add(new UserSignal("zajęty"));
            UserSignals.Add(new UserSignal("koniec"));
            UserSignals.Add(new UserSignal("początek"));

            FileProvider.LoadData(UserSignals);

        }

        public void Flash(string message, CancellationToken cancelToken, MediaElement mediaPlayer)
        {
            //MediaPlayer = new MediaElement();
            //MediaPlayer.AutoPlay = false;
            //var folder = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync(@"Assets");
            //SoundFile = await folder.GetFileAsync("signalSound");
            //var stream = await SoundFile.OpenAsync(Windows.Storage.FileAccessMode.Read);
            //MediaPlayer.SetSource(stream, "");


            var signal = Signal.GetInstane();
            isMessageFlashing = true;
            bool firstIt = true;
            while (IsRepeat || firstIt && !cancelToken.IsCancellationRequested)
            {
                firstIt = false;
                foreach (var letter in message.ToCharArray())
                {
                    foreach (var item in signal.GetSignal(letter))
                    {
                        if (cancelToken.IsCancellationRequested)
                            return;
                        if (!cancelToken.IsCancellationRequested)
                            Delay(FlashSpan, cancelToken);
                        if (isMessageFlashing)
                        {
                            if (isLightEnabled)
                                FlashLightInstance.TurnOn();
                            if (isSoundEnabled)
                                BackgroundMediaPlayer.Current.Play();
                        }

                        if (cancelToken.IsCancellationRequested)
                        {
                            FlashLightInstance.TurnOff();
                            BackgroundMediaPlayer.Current.Pause();
                            return;
                        }
                        if (!cancelToken.IsCancellationRequested)
                            Delay(item, cancelToken);
                        if (isMessageFlashing)
                        {
                            FlashLightInstance.TurnOff();
                            BackgroundMediaPlayer.Current.Pause();
                        }

                    }
                    if (cancelToken.IsCancellationRequested)
                    {
                        return;
                    }
                }
                if (!cancelToken.IsCancellationRequested)
                    Delay(TimespanBetweenRepeats, cancelToken);
                else
                    return;
            }


        }

        public bool Delay(double timestep, CancellationToken cancelToken)
        {
            DateTime start = DateTime.Now;
            while (DateTime.Now.Subtract(start).Milliseconds < (timestep * 290))
            {
                Debug.WriteLine(DateTime.Now.Subtract(start).Milliseconds);
                if (cancelToken.IsCancellationRequested)
                    return false;
            }
            return false;

        }

        public void FlashOn()
        {
            if (isLightEnabled)
                FlashLightInstance.TurnOn();
            isConstantLightOn = true;
            isMessageFlashing = false;
        }

        public void FlashOff()
        {
            FlashLightInstance.TurnOff();
            isConstantLightOn = false;
            isMessageFlashing = false;
        }

        public void SetBrighter()
        {
            if (FlashLightInstance.GetBrightness() <= 0.8)
            {
                FlashLightInstance.SetBrighter();
            }
        }

        public void SetDarker()
        {
            if (FlashLightInstance.GetBrightness() >= 0.2f)
            {
                FlashLightInstance.SetDarker();
            }
        }

        public void FlashFaster()
        {
            var signal = Signal.GetInstane();
            if (signal.ShortSignal > 0.5)
            {
                signal.UpdateSignalsLengts(signal.LongSignal - TimeSpan, signal.ShortSignal - TimeSpan);
                FlashSpan -= 0.1;
            }

        }
        public void FlashSlower()
        {
            var signal = Signal.GetInstane();
            if (signal.ShortSignal < 1)
            {
                signal.UpdateSignalsLengts(signal.LongSignal + TimeSpan, signal.ShortSignal + TimeSpan);
                FlashSpan += 0.1;
            }

        }

        public void Abort()
        {
            isAborted = true;
        }

        public void AddSignal(string signal)
        {
            UserSignals.Add(new UserSignal(signal));
            FileProvider.SaveData(UserSignals);
            //FileProvider.LoadData(UserSignals);
        }

        public void RemoveSignal(string signal)
        {
            UserSignals.Remove(UserSignals.Where(x => x.Content == signal).FirstOrDefault());
            FileProvider.SaveData(UserSignals);
        }

    }
}
