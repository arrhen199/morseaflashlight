﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseaFlashlight.Morsea
{
    public interface IFlashLight
    {
        void TurnOn();
        void TurnOff();
        void SetBrighter();
        void SetDarker();
        double GetBrightness();

    }
}
