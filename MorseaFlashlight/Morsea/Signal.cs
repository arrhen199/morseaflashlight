﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MorseaFlashlight.Morsea
{
    class Signal
    {

        public double LongSignal { get; set; }
        public double ShortSignal { get; set; }

        private static Signal _Signal;

        private Dictionary<char, IEnumerable<double>> SignalsMap;

        private Signal(int LongSignals = 2, int ShortSignals = 1)
        {
            SignalsMap = new Dictionary<char, IEnumerable<double>>();
            LongSignal = LongSignals;
            ShortSignal = ShortSignals;
            setUpSignalsMap();
        }

        public static Signal GetInstane(int LongSignal = 2, int ShortSignal = 1)
        {
            if (_Signal == null)
                _Signal = new Signal(LongSignal, ShortSignal);

            return _Signal;
        }

        public IEnumerable<double> GetSignal(char x)
        {
            IEnumerable<double> result;
            if (SignalsMap.TryGetValue(x, out result))
            {
                return result;
            }
            else
            {
                throw new Exception(" Missing char in map ");
            }

        }

        private void setUpSignalsMap()
        {
            SignalsMap.Add('a', new List<double>() { ShortSignal, LongSignal });
            SignalsMap.Add('ą', new List<double>() { ShortSignal, LongSignal });
            SignalsMap.Add('b', new List<double>() { LongSignal, ShortSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('c', new List<double>() { LongSignal, ShortSignal, LongSignal, ShortSignal });
            SignalsMap.Add('ć', new List<double>() { LongSignal, ShortSignal, LongSignal, ShortSignal });
            SignalsMap.Add('d', new List<double>() { LongSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('e', new List<double>() { ShortSignal });
            SignalsMap.Add('ę', new List<double>() { ShortSignal });
            SignalsMap.Add('f', new List<double>() { ShortSignal, ShortSignal, LongSignal, ShortSignal });
            SignalsMap.Add('g', new List<double>() { LongSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('h', new List<double>() { ShortSignal, ShortSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('i', new List<double>() { ShortSignal, ShortSignal });
            SignalsMap.Add('j', new List<double>() { ShortSignal, LongSignal, LongSignal, LongSignal });
            SignalsMap.Add('k', new List<double>() { LongSignal, ShortSignal, LongSignal });
            SignalsMap.Add('l', new List<double>() { ShortSignal, LongSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('m', new List<double>() { LongSignal, LongSignal });
            SignalsMap.Add('n', new List<double>() { LongSignal, ShortSignal });
            SignalsMap.Add('ń', new List<double>() { LongSignal, ShortSignal });
            SignalsMap.Add('o', new List<double>() { LongSignal, LongSignal, LongSignal });
            SignalsMap.Add('ó', new List<double>() { LongSignal, LongSignal, LongSignal });
            SignalsMap.Add('p', new List<double>() { ShortSignal, LongSignal, LongSignal, ShortSignal });
            SignalsMap.Add('r', new List<double>() { ShortSignal, LongSignal, ShortSignal });

            SignalsMap.Add('s', new List<double>() { ShortSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('ś', new List<double>() { ShortSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('t', new List<double>() { LongSignal });
            SignalsMap.Add('u', new List<double>() { ShortSignal, ShortSignal, LongSignal });
            SignalsMap.Add('w', new List<double>() { ShortSignal, LongSignal, LongSignal });
            SignalsMap.Add('v', new List<double>() { ShortSignal, ShortSignal, ShortSignal, LongSignal });
            SignalsMap.Add('x', new List<double>() { LongSignal, ShortSignal, ShortSignal, LongSignal });
            SignalsMap.Add('y', new List<double>() { LongSignal, ShortSignal, LongSignal });
            SignalsMap.Add('z', new List<double>() { LongSignal, LongSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('ź', new List<double>() { LongSignal, LongSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('ż', new List<double>() { LongSignal, LongSignal, ShortSignal, ShortSignal });

            SignalsMap.Add('0', new List<double>() { LongSignal, LongSignal, LongSignal, LongSignal, LongSignal });
            SignalsMap.Add('1', new List<double>() { ShortSignal, LongSignal, LongSignal, LongSignal, LongSignal });
            SignalsMap.Add('2', new List<double>() { ShortSignal, ShortSignal, LongSignal, LongSignal, LongSignal });
            SignalsMap.Add('3', new List<double>() { ShortSignal, ShortSignal, ShortSignal, LongSignal, LongSignal });
            SignalsMap.Add('4', new List<double>() { ShortSignal, ShortSignal, ShortSignal, ShortSignal, LongSignal });
            SignalsMap.Add('5', new List<double>() { ShortSignal, ShortSignal, ShortSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('6', new List<double>() { LongSignal, ShortSignal, ShortSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('7', new List<double>() { LongSignal, LongSignal, ShortSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('8', new List<double>() { LongSignal, LongSignal, LongSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('9', new List<double>() { LongSignal, LongSignal, LongSignal, LongSignal, ShortSignal });
            SignalsMap.Add(' ', new List<double>() { 3 });

            SignalsMap.Add('.', new List<double>() { ShortSignal, LongSignal, ShortSignal, LongSignal, ShortSignal, LongSignal });
            SignalsMap.Add(',', new List<double>() { LongSignal, LongSignal, ShortSignal, ShortSignal, LongSignal, LongSignal });
            SignalsMap.Add('\'', new List<double>() { ShortSignal, LongSignal, LongSignal, LongSignal, LongSignal, ShortSignal });
            SignalsMap.Add('\"', new List<double>() { ShortSignal, LongSignal, ShortSignal, ShortSignal, LongSignal, ShortSignal });
            SignalsMap.Add('_', new List<double>() { ShortSignal, ShortSignal, LongSignal, LongSignal, ShortSignal, LongSignal });
            SignalsMap.Add(':', new List<double>() { LongSignal, LongSignal, LongSignal, ShortSignal, ShortSignal, ShortSignal });
            SignalsMap.Add(';', new List<double>() { LongSignal, ShortSignal, LongSignal, ShortSignal, LongSignal, ShortSignal });
            SignalsMap.Add('?', new List<double>() { ShortSignal, ShortSignal, LongSignal, LongSignal, ShortSignal, ShortSignal });
            SignalsMap.Add('!', new List<double>() { LongSignal, ShortSignal, LongSignal, ShortSignal, LongSignal, LongSignal });
            SignalsMap.Add('-', new List<double>() { LongSignal, ShortSignal, ShortSignal, ShortSignal, ShortSignal, LongSignal });
            SignalsMap.Add('+', new List<double>() { ShortSignal, LongSignal, ShortSignal, LongSignal, ShortSignal });
            SignalsMap.Add('/', new List<double>() { LongSignal, ShortSignal, ShortSignal, LongSignal, ShortSignal });
            SignalsMap.Add('(', new List<double>() { LongSignal, ShortSignal, LongSignal, LongSignal, ShortSignal });
            SignalsMap.Add(')', new List<double>() { LongSignal, ShortSignal, LongSignal, LongSignal, ShortSignal, LongSignal });
            SignalsMap.Add('=', new List<double>() { LongSignal, ShortSignal, ShortSignal, ShortSignal, LongSignal });
            SignalsMap.Add('@', new List<double>() { ShortSignal, LongSignal, LongSignal, ShortSignal, LongSignal, ShortSignal });

        }

        public void UpdateSignalsLengts(double LongSignal, double ShortSignal)
        {
            _Signal.LongSignal = LongSignal;
            _Signal.ShortSignal = ShortSignal;
            _Signal.SignalsMap.Clear();
            setUpSignalsMap();
        }
    }
}
